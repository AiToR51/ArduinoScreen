#include "UTFT/UTFT.h"
#include "UTFT/DefaultFonts.h"
#include "UTouch/UTouch.h"
#include <Arduino.h>
#include "ECU/ECU.h"
#include "ECU/MS/MS2.h"
#include "ECU/FreeEMS/FreeEMS.h"
#include "Adafruit_MAX31855/Adafruit_MAX31855.h"

#define LED_RED 10
#define LED_ORANGE 11
#define LED_GREEN 12

#define TEST_MODE false

UTFT myGLCD(ILI9481, 38, 39, 40, 41);
//UTouch myTouch(6, 5, 4, 3, 2);

// Declare which fonts we will be using
//extern unsigned char BigFont[];
//extern unsigned char SevenSegNumFont[];
extern unsigned char ArialNumFontPlus[];

int x, y;

int currentY = 0;
int currentX = 0;
#define ITEM_HEIGHT 60

#define START_C1_BAR_STRUCT 120
#define FIRST_X_BAR 155
#define LAST_X_BAR 375
#define BAR_HEIGHT 15

int currentRPM = 0;
int currentEGOCORR = 0;
int currentKnock = 0;
int currentEthanol = 0;
int currentMAT = 0;
int currentCLT = 0;
double currentAFR = 0.0;
int currentMAP = 0;
double currentBattVolt = 0.0;

int currentEGT[] = { 0, 0, 0, 0 };

int lastDrawRPM = -1;
int lastDrawEGOCOR = -1;
int lastDrawMAT = -1;
int lastDrawCLT = -1;
double lastDrawAFR = -1;
double lastDrawBattVolt = -1;
int lastDrawMAP = -1;
int lastDrawKnock = 0;
int lastDrawEthanol = 0;

int lastDrawEGT[] = { -1, -1, -1, -1 };

ECU* ecu;

//EGT

#define CLK  2
#define DO   3
#define CS1   4
#define CS2   5
#define CS3   6
#define CS4   7
Adafruit_MAX31855 thermocouples[] = { Adafruit_MAX31855(CLK, CS1, DO),
		Adafruit_MAX31855(CLK, CS2, DO), Adafruit_MAX31855(CLK, CS3, DO),
		Adafruit_MAX31855(CLK, CS4, DO) };

int getBarLength(double minValue, double maxValue, double currentValue) {
	return min(
			max( ((currentValue - minValue) * (LAST_X_BAR - FIRST_X_BAR) / (maxValue - minValue)), 0),
			LAST_X_BAR - FIRST_X_BAR);
}
void setColorBasedOnIntervals(double minSafeValue, double maxSafeValue,
		double alarmOver, double currentValue) {
	if (currentValue >= minSafeValue && currentValue <= maxSafeValue) {
		myGLCD.setColor(0, 200, 0);
	} else if (currentValue >= alarmOver) {
		myGLCD.setColor(200, 0, 0);
	} else {
		myGLCD.setColor(230, 150, 30);
	}
}

void drawStructureForBar(int position, int xPosition, char * title,
		char * units) {
	myGLCD.setColor(255, 255, 255);
	currentY = position * 30;
	currentX = xPosition;

	myGLCD.print(title, currentX, currentY, 0);
	myGLCD.print(units, currentX + 133, currentY, 0);
	myGLCD.drawRoundRect(FIRST_X_BAR - 1, currentY, LAST_X_BAR + 1,
			currentY + BAR_HEIGHT + 1);
}

void drawStructureForNumber(int yPosition, int xPosition, char * title) {
	myGLCD.setColor(255, 255, 255);
	currentY = yPosition * 30;
	currentX = xPosition * 160;
	myGLCD.drawRoundRect(currentX + 10, currentY + 10, currentX + 150,
			currentY + 90);
	myGLCD.print(title, currentX + 55, currentY, 0);
}

void drawSmallSideStructureForNumber(int yPosition, int xPosition,
		char * title) {
	myGLCD.setColor(255, 255, 255);
	currentY = yPosition * 35;
	currentX = LAST_X_BAR;
	myGLCD.drawRoundRect(currentX + 10, currentY + 10, currentX + 100,
			currentY + 60);
	myGLCD.print(title, currentX + 30, currentY, 0);
}

void drawIntValueWithBar(int rowPosition, int columnPosition, int currentValue,
		int length, int minValue, int maxValue, int minSafeValue,
		int maxSafeValue, int alarmOver) {
	if (abs(currentValue) >= pow(10, length)) {
		currentValue = 0;
	}
	currentY = rowPosition * 30;
	myGLCD.setColor(255, 255, 255);
	myGLCD.printNumI(currentValue, 82, currentY, length, ' ');

	int barLenght = getBarLength(minValue, maxValue, currentValue);

	setColorBasedOnIntervals(minSafeValue, maxSafeValue, alarmOver,
			currentValue);
	myGLCD.fillRect(FIRST_X_BAR, currentY + 1, FIRST_X_BAR + barLenght,
			currentY + BAR_HEIGHT);
	myGLCD.setColor(0, 0, 100);
	myGLCD.fillRect(FIRST_X_BAR + barLenght, currentY + 1, LAST_X_BAR,
			currentY + BAR_HEIGHT);
}

void drawFloatValueWithBar(int position, double currentValue, int length,
		double minValue, double maxValue, double minSafeValue,
		double maxSafeValue, double alarmOver) {
	if (abs(currentValue) >= pow(10, length)) {
		currentValue = 0;
	}
	currentY = position * 30;
	myGLCD.setColor(255, 255, 255);
	myGLCD.printNumF(currentValue, 1, 82, currentY, '.', length, ' ');

	int barLenght = getBarLength(minValue, maxValue, currentValue);

	setColorBasedOnIntervals(minSafeValue, maxSafeValue, alarmOver,
			currentValue);
	myGLCD.fillRect(FIRST_X_BAR, currentY + 1, FIRST_X_BAR + barLenght,
			currentY + BAR_HEIGHT);
	myGLCD.setColor(0, 0, 100);
	myGLCD.fillRect(FIRST_X_BAR + barLenght, currentY + 1, LAST_X_BAR,
			currentY + BAR_HEIGHT);
}

void drawIntValueWithBar(int rowPosition, int currentValue, int length,
		int minValue, int maxValue, int minSafeValue, int maxSafeValue,
		int alarmOver) {
	drawIntValueWithBar(rowPosition, 0, currentValue, length, minValue,
			maxValue, minSafeValue, maxSafeValue, alarmOver);
}

void drawFloatValue(int yPosition, int xPosition, double currentValue) {
	currentY = yPosition * 30;
	currentX = xPosition * 160;
	myGLCD.setFont((uint8_t*) SevenSegNumFont);
	myGLCD.setColor(255, 255, 255);
	myGLCD.printNumF(currentValue, 1, currentX + 15, currentY + 25, '.', 2,
			'0');
	myGLCD.setFont((uint8_t*) BigFont);
}

void drawIntValue(int yPosition, int xPosition, int currentValue) {
	currentY = yPosition * 30;
	currentX = xPosition * 160;
	myGLCD.setFont((uint8_t*) SevenSegNumFont);
	myGLCD.setColor(255, 255, 255);
	myGLCD.printNumI(currentValue, currentX + 40, currentY + 25, 3, '0');
	myGLCD.setFont((uint8_t*) BigFont);
}

void drawSideIntValue(int yPosition, int xPosition, int currentValue,
		int length) {
	currentY = yPosition * 35;
	currentX = LAST_X_BAR;
	myGLCD.setColor(255, 255, 255);
	int addX = 0;
	if (length == 2) {
		addX = 35;
	} else if (length == 3) {
		addX = 30;
	} else if (length == 4) {
		addX = 25;
	}
	myGLCD.printNumI(currentValue, currentX + addX, currentY + 25, length, ' ');
}
/*  myGLCD.setColor(0, 0, 255);
 myGLCD.fillRoundRect (10+(x*60), 10, 60+(x*60), 60);
 myGLCD.setColor(255, 255, 255);
 myGLCD.drawRoundRect (10+(x*60), 10, 60+(x*60), 60);
 myGLCD.printNumI(x+1, 27+(x*60), 27);*/

/*
 myGLCD.setColor(0, 0, 255);
 myGLCD.fillRoundRect (10, 130, 150, 180);
 myGLCD.setColor(255, 255, 255);
 myGLCD.drawRoundRect (10, 130, 150, 180);
 myGLCD.print("Clear", 40, 147);
 myGLCD.setColor(0, 0, 255);
 myGLCD.fillRoundRect (160, 130, 300, 180);
 myGLCD.setColor(255, 255, 255);
 myGLCD.drawRoundRect (160, 130, 300, 180);
 myGLCD.print("Enter", 190, 147);
 myGLCD.setBackColor (0, 0, 0);*/

void drawButtons() {
	myGLCD.setBackColor(5, 20, 120);
	myGLCD.setColor(5, 20, 120);
	myGLCD.fillRoundRect(10, 260, 150, 310);
	myGLCD.setColor(255, 255, 255);
	myGLCD.drawRoundRect(10, 260, 150, 310);
	myGLCD.print("Menu", 50, 275);

	myGLCD.setColor(5, 20, 120);
	myGLCD.fillRoundRect(260, 260, 470, 310);
	myGLCD.setColor(255, 255, 255);
	myGLCD.drawRoundRect(260, 260, 470, 310);
	myGLCD.print("Stop Alarma", 280, 275);

	myGLCD.setBackColor(0, 0, 0);
}
/*
 // Draw a red frame while a button is touched
 void waitForIt(int x1, int y1, int x2, int y2) {
 myGLCD.setColor(255, 0, 0);
 myGLCD.drawRoundRect(x1, y1, x2, y2);
 while (myTouch.dataAvailable())
 myTouch.read();
 myGLCD.setColor(255, 255, 255);
 myGLCD.drawRoundRect(x1, y1, x2, y2);
 }*/

void printFirstScreen() {
	myGLCD.setColor(100, 50, 255);
	myGLCD.print("Benvido Aitor!", CENTER, 80);
	myGLCD.setColor(30, 100, 150);
	delay(500);
	myGLCD.print("Intentando conectar...", CENTER, 120);
	delay(500); //removeme
}

void serialFlush() {
	while (Serial1.available() > 0)
		Serial1.read();
}

boolean connectBySerial() {
	boolean connected = true;
	//Serial1.begin(115200);
	delay(100);
	char version[14] = "Not detected";
	serialFlush();
	while (!connected) {
		Serial1.write('S');
		delay(100);
		if (Serial1.available() > 14) {
			Serial1.readBytes(version, 14);
			if (version[0] == 'M' && version[1] == 'S') {
				connected = true;
			}
			//  Serial.write(version,20);
		}
		serialFlush();
		//try connect
		//connected = true; //FIXME
		if (connected) {
			myGLCD.setColor(0, 255, 0);
			myGLCD.print("     Conexion OK.      ", CENTER, 160);
			myGLCD.print(version, CENTER, 200);
			Serial.println("nada");

		} else {
			myGLCD.setColor(255, 0, 0);

			myGLCD.print("ERROR. Revise conexion.", CENTER, 160);
			Serial.println("ok");

		}
		delay(1000);
	}

	return connected;
}

void printMainScreen() {
	myGLCD.clrScr();
	drawStructureForBar(0, 0, "T.Ag:", "C");
	drawStructureForBar(1, 0, "T.Ad:", "C");
	drawStructureForBar(2, 0, "RPM:", "");
	drawStructureForBar(3, 0, "EGT1:", "C");
	drawStructureForBar(4, 0, "EGT2:", "C");
	drawStructureForBar(5, 0, "EGT3:", "C");
	drawStructureForBar(6, 0, "EGT4:", "C");
	drawStructureForNumber(7, 0, "AFR");
	drawStructureForNumber(7, 1, "VOL");
	drawStructureForNumber(7, 2, "MAP");
	drawSmallSideStructureForNumber(0, 0, "ETH");
	drawSmallSideStructureForNumber(2, 0, "EGC");
	drawSmallSideStructureForNumber(4, 0, "KNK");

	//drawButtons();
}

void setShiftLights(int rpm) {
	if (rpm > 6200) {
		digitalWrite(LED_GREEN, LOW);
		if (rpm > 6500) {
			digitalWrite(LED_ORANGE, LOW);
			if (rpm > 6800) {
				digitalWrite(LED_RED, LOW);
				if (rpm > 7000) {
					//TODO parpadeo
				}
			} else {
				digitalWrite(LED_RED, HIGH);
			}
		} else {
			digitalWrite(LED_RED, HIGH);
			digitalWrite(LED_ORANGE, HIGH);
		}
	} else {
		digitalWrite(LED_RED, HIGH);
		digitalWrite(LED_ORANGE, HIGH);
		digitalWrite(LED_GREEN, HIGH);
	}
}
void setup() {
	// Initial setup
	//Serial.begin(115200);
	delay(500);
	pinMode(LED_RED, OUTPUT);
	pinMode(LED_ORANGE, OUTPUT);
	pinMode(LED_GREEN, OUTPUT);
	digitalWrite(LED_RED, HIGH);
	digitalWrite(LED_ORANGE, HIGH);
	digitalWrite(LED_GREEN, HIGH);
	delay(300);
	digitalWrite(LED_GREEN, LOW);
	delay(300);
	digitalWrite(LED_ORANGE, LOW);
	delay(300);
	digitalWrite(LED_RED, LOW);

	myGLCD.InitLCD(LANDSCAPE);
	myGLCD.clrScr();
	//myTouch.InitTouch();
	//myTouch.setPrecision(PREC_LOW);

	myGLCD.setFont((uint8_t*) BigFont);
	myGLCD.setBackColor(0, 0, 0);
	printFirstScreen();
	ecu = new MS2(&Serial1);
	if (!TEST_MODE) {
		connectBySerial();
	}

	delay(300);
	digitalWrite(LED_RED, HIGH);
	delay(300);
	digitalWrite(LED_ORANGE, HIGH);
	delay(300);
	digitalWrite(LED_GREEN, HIGH);
	delay(300);
	printMainScreen();
}

int fahToCelsius(int fah) {
	return (fah - 32) * 0.5556;
}

void loop() {
	if (!TEST_MODE) {
		ecu->update();
		currentAFR = ecu->getAFR();
		currentCLT = ecu->getCLT();
		currentMAP = ecu->getMAP();
		currentRPM = ecu->getRPM();
		currentMAT = ecu->getMAT();
		currentEGOCORR = ecu->getEGOCorr();
		currentBattVolt = ecu->getBattVolt();
		currentKnock = ecu->getKnockInput() * 100 + ecu->getKnockRetard();
		currentEthanol = ecu->getFlexFuelCorrection();
	} else {
		currentAFR = 13.3;
		currentBattVolt = 14.2;
		currentCLT = 100;
		currentMAT = 35;
		currentMAP = 189;

		currentRPM = 3676;
		currentEGOCORR = 110;
		currentKnock = 8602;
		currentEthanol = 85;

	}



	if (lastDrawRPM != currentRPM) {
		drawIntValueWithBar(2, currentRPM, 4, 100, 7200, 700, 6500, 7000);
		lastDrawRPM = currentRPM;
		setShiftLights(currentRPM);
	}
	if (lastDrawCLT != currentCLT) {
		drawIntValueWithBar(0, currentCLT, 3, 40, 110, 80, 95, 100);
		lastDrawCLT = currentCLT;
	}
	if (lastDrawMAT != currentMAT) {
		drawIntValueWithBar(1, currentMAT, 3, 10, 60, 0, 40, 60);
		lastDrawMAT = currentMAT;
	}

	if (lastDrawAFR != currentAFR) {
		drawFloatValue(7, 0, currentAFR);
		lastDrawAFR = currentAFR;
	}

	if (lastDrawBattVolt != currentBattVolt) {
		drawFloatValue(7, 1, currentBattVolt);
		lastDrawBattVolt = currentBattVolt;
	}

	if (lastDrawMAP != currentMAP) {
		drawIntValue(7, 2, currentMAP);
		lastDrawMAP = currentMAP;
	}

	if (lastDrawEthanol != currentEthanol) {
		drawSideIntValue(0, 0, currentEthanol, 2);
		lastDrawEthanol = currentEthanol;
	}

	if (lastDrawEGOCOR != currentEGOCORR) {
		drawSideIntValue(2, 0, currentEGOCORR, 3);
		lastDrawEGOCOR = currentEGOCORR;
	}
	if (lastDrawKnock != currentKnock) {
		drawSideIntValue(4, 0, currentKnock, 4);
		lastDrawKnock = currentKnock;
	}

	for (int i = 0; i < 4; i++) {
		Adafruit_MAX31855 thermocouple = thermocouples[i];

		double c = thermocouple.readCelsius();
		if (isnan(c)) {
			currentEGT[i] = 0;
		} else {
			currentEGT[i] = (int) c;
		}
		if (currentEGT[i] > 1000) {
			currentEGT[i] = 999;
		}
		if (currentEGT[i] <= -100) {
			currentEGT[i] = 999;
		}

		if (TEST_MODE) {
			currentEGT[i] = 500;
		}
		if (lastDrawEGT[i] != currentEGT[i]) {
			drawIntValueWithBar(3 + i, currentEGT[i], 3, 250, 900, 400, 700,
					800);
			lastDrawEGT[i] = currentEGT[i];
		}
	}

	/*
	 if (myTouch.dataAvailable()) {
	 myTouch.read();
	 x = myTouch.getX();
	 y = myTouch.getY();
	 if (y > 260) {
	 if (x < 150) {
	 //boton menu
	 myGLCD.setColor(255, 25, 200);
	 Serial.println("MENU");
	 myGLCD.print("No hay menu :(  ", 140, 230);
	 } else if (x > 250) {
	 //borrar alarmas
	 Serial.println("borrar");
	 myGLCD.setColor(255, 0, 0);
	 myGLCD.print("Te quiero muchito  ", 140, 230);
	 }
	 }
	 }*/

	/*
	 if (i%10 == 0) {
	 drawIntValueWithBar(0,random(87,90),3,-10,105,80,95,100);
	 }
	 if (i %3 == 0) {
	 drawIntValueWithBar(1,random(40,60),3,10,60,0,40,60);
	 }
	 drawIntValueWithBar(2,random(0,7000),4,0,7000,700,6500,6800);

	 drawFloatValue(3,0,((double)random(100,200))/10);
	 //drawIntValue(3,2,random(20,300));
	 drawIntValue(3,1,i);
	 i++;
	 i = i%100;

	 */

}
