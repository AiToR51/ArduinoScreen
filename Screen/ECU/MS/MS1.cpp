/*
 * MS1.cpp
 *
 *  Created on: 12 de may. de 2016
 *      Author: ATR
 */
#include "MS1.h"

int temperatureSensor[] = {
#include "temperature/FORD_SENSOR.h"
		};

MS1::MS1(HardwareSerial* serialPort) {
	_serialPort = serialPort;
	_serialPort->begin(9600);
}

MS1::~MS1() {

}

void MS1::serialFlush() {
	while (_serialPort->available() > 0) {
		_serialPort->read();
	}
}

void MS1::update() {
	delay(50);
	if (_serialPort->available() == 0) {
		_serialPort->print('A');
	} else if (_serialPort->available() < 22) {
		delay(50);
	}

	if (_serialPort->available() == 22) {
		byte readBytes = _serialPort->readBytes(buffer, 22);
		if (readBytes == 22) {
			int i = 0;
			currentMAP = raw2MAP(buffer[4]);
			currentMAT = raw2Temp(buffer[5]);
			currentCLT = raw2Temp(buffer[6]);
			currentAFR = raw2EGO(buffer[9]);
			currentRPM = raw2RPM(buffer[13]);
		}
	}
	serialFlush();
	_serialPort->print('A');
}

int MS1::raw2Temp(uint16_t value) {
	return temperatureSensor[value];
}

int MS1::raw2Percent(uint16_t value) {
	return value / 640;
}

int MS1::raw2MAP(uint16_t value) {
	return value + 10;
}

int MS1::raw2RPM(uint16_t value) {
	return value * 100;
}

double MS1::raw2EGO(uint16_t value) {
	return 10 + (value / 25.5);
}

