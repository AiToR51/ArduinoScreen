#ifndef MS2_h
#define MS2_h
#include "../ECU.h"

class MS2: public ECU {

public:
	void update();

	~MS2();
	MS2(HardwareSerial* serialPort);
private:
	uint8_t buffer[80];
	HardwareSerial* _serialPort;
	int raw2Temp(uint16_t value);
	int raw2Percent(uint16_t value);
	int raw2MAP(uint16_t value);
	int raw2RPM(uint16_t value);
	double raw2Double(uint16_t value);
	double raw2EGO(uint16_t value);
	void serialFlush();
};

#endif
