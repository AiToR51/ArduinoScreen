/*
 * MS2.cpp
 *
 *  Created on: 12 de may. de 2016
 *      Author: ATR
 */
#include "MS2.h"

MS2::MS2(HardwareSerial* serialPort) {
	_serialPort = serialPort;
	_serialPort->begin(115200);
}

MS2::~MS2() {

}

void MS2::serialFlush() {
	while (_serialPort->available() > 0) {
		_serialPort->read();
	}
}

void MS2::update() {
	if (_serialPort->available() == 0) {
		delay(30);
	}
	if (_serialPort->available() < 30) {
		delay(15);
	}

	if (_serialPort->available() > 29) {
		byte readBytes = _serialPort->readBytes(buffer, 75);
		if (readBytes >= 30) {
			int tempCLT = raw2Temp((buffer[22] << 8) + buffer[23]);
			currentBatVolt = raw2EGO((buffer[26] << 8) + buffer[27]);
			double tempAFR = raw2EGO((buffer[28] << 8) + buffer[29]);
			if (tempAFR > 21 | tempAFR < 9 || tempCLT < -15) { //chapuza de filtrado
				return;
			}
			currentMAP = raw2MAP((buffer[18] << 8) + buffer[19]);
			currentMAT = raw2Temp((buffer[20] << 8) + buffer[21]);
			currentCLT = tempCLT;
			currentAFR = min(tempAFR, 20);
			currentRPM = raw2RPM((buffer[6] << 8) + buffer[7]);

			if (readBytes >= 72) {
				currentKnockInput = raw2Percent((buffer[32] << 8) + buffer[33]);
				currentKnockRetard = raw2Double((buffer[71] << 8) + buffer[72]);
				currentFlexCorrection = (buffer[68] << 8) + buffer[69];
			}
			currentEGOCorr = raw2Percent((buffer[34] << 8) + buffer[35]);
		}
	}
	delay(20);
	serialFlush();
	_serialPort->print('A');
}

int MS2::raw2Temp(uint16_t value) {
	return (value / 10 - 32) * 0.5556;
}

int MS2::raw2Percent(uint16_t value) {
	return value / 10;
}

double MS2::raw2Double(uint16_t value) {
	return value / 10.0;
}

int MS2::raw2MAP(uint16_t value) {
	return value / 10;
}

int MS2::raw2RPM(uint16_t value) {
	return value;
}

double MS2::raw2EGO(uint16_t value) {
	return value / 10.0;
}

