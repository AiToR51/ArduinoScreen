#ifndef MS1_h
#define MS1_h
#include "../ECU.h"

class MS1: public ECU {

public:
	void update();

	~MS1();
	MS1(HardwareSerial* serialPort);
private:
	uint8_t buffer[40];
	HardwareSerial* _serialPort;
	int raw2Temp(uint16_t value);
	int raw2Percent(uint16_t value);
	int raw2MAP(uint16_t value);
	int raw2RPM(uint16_t value);
	double raw2EGO(uint16_t value);
	void serialFlush();
};

#endif
