#ifndef ECU_h
#define ECU_h

#include "Arduino.h"
class ECU {
public:
	// pure virtual function
	int getRPM() {
		return currentRPM;
	}
	int getMAP() {
		return currentMAP;
	}
	int getMAT() {
		return currentMAT;
	}
	int getCLT() {
		return currentCLT;
	}

	int getEGOCorr() {
		return currentEGOCorr;
	}
	double getAFR() {
		return currentAFR;
	}

	double getBattVolt() {
		return currentBatVolt;
	}

	int getKnockInput() {
		return currentKnockInput;
	}

	double getKnockRetard() {
		return currentKnockRetard;
	}

	int getFlexFuelCorrection() {
		return currentFlexCorrection;
	}

	virtual void update();

	~ECU() {

	}

protected:
	int currentRPM = 0;
	int currentMAT = 0;
	int currentCLT = 0;
	int currentEGOCorr = 0;
	int currentKnockInput = 0;
	double currentKnockRetard = 0.0;
	double currentAFR = 0.0;
	double currentBatVolt = 0.0;
	int currentFlexCorrection = 0;

	int currentMAP = 0;
};

#endif
