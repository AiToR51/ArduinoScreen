/*
 * FreeEMS.cpp
 *
 *  Created on: 12 de may. de 2016
 *      Author: ATR
 */
#include "FreeEMS.h"

FreeEMS::FreeEMS(HardwareSerial* serialPort) {
	_serialPort = serialPort;
	_serialPort->begin(115200);
}

FreeEMS::~FreeEMS() {

}

void FreeEMS::update() {

	if (_serialPort->available()) {
		byte readBytes = _serialPort->readBytes(buffer, 255);
		uint8_t checksum = 0;
		boolean foundStart = false;
		boolean foundEnd = false;
		uint8_t startPos = 0;
		uint8_t endPos = 0;
		uint8_t obtainedChecksum = 0x00;
		for (int i = 0; i < readBytes; i++) {
			//Serial.print(buffer[i], HEX);
			//Serial.print(" ");
			if (!foundStart && buffer[i] == 0xAA) {
				foundStart = true;
				startPos = i + 1;
			} else if (foundStart && !foundEnd && buffer[i] == 0xCC) {
				foundEnd = true;
				obtainedChecksum = buffer[i - 1];
				endPos = i - 2;
				if (obtainedChecksum == checksum) {
					processRawData(&buffer[startPos], endPos - startPos);
					//Serial.print("Paquete OK, se procesa ");
					//Serial.println(endPos - startPos);
				} else {
					//Serial.println("Se rechaza paquete por mal checksum");
				}

			}
			if (foundStart && !foundEnd && i < (readBytes - 1)
					&& buffer[i + 1] != 0xCC && buffer[i] != 0xAA) {
				checksum += buffer[i];
			}
		}
	}
}

void FreeEMS::processRawData(uint8_t * buffer, uint16_t length) {
	if (length >= 20) {
		uint8_t deEscapedMessage[length];
		//descape
		int newIndex = 0;
		for (int i = 0; i < length; i++) {
			uint8_t value = buffer[i];
			if (value == 0xBB && (i < length - 1)) {
				i++;
				value = buffer[i];
				if (value == 0x33) {
					value = 0xCC;
				} else if (value == 0x44) {
					value = 0xBB;
				} else if (value == 0x55) {
					value = 0xAA;
				} else {
					return;
				}
				i++;
			}
			deEscapedMessage[newIndex] = value;
			newIndex++;
		}
		length = newIndex;
		int startPayloadPos = 5;
		//process head
		if (deEscapedMessage[0] == 0x01) {
			//sin seq
		} else if (deEscapedMessage[0] == 0x03) {
			//con seq
			startPayloadPos = 6;
		}
		uint16_t payloadId = (deEscapedMessage[1] << 8) + deEscapedMessage[2];
		if (payloadId == 0x191) {
			setData(&deEscapedMessage[startPayloadPos]);
		}
	}

}

void FreeEMS::setData(uint8_t * message) {
	Serial.print("rawMAT ");
	Serial.print(message[0], HEX);
	Serial.print(" - ");
	Serial.println(message[1], HEX);
	currentMAT = raw2Temp((message[0] << 8) + message[1]);
	currentCLT = raw2Temp((message[2] << 8) + message[3]);
	currentAFR = raw2EGO((message[6] << 8) + message[7]);
	currentMAP = raw2MAP((message[8] << 8) + message[9]);
	currentRPM = raw2RPM((message[26] << 8) + message[27]);
}

int FreeEMS::raw2Temp(uint16_t value) {
	return (value - 27315) / 100;
}

int FreeEMS::raw2Percent(uint16_t value) {
	return value / 640;
}

int FreeEMS::raw2MAP(uint16_t value) {
	return value / 100;
}

int FreeEMS::raw2RPM(uint16_t value) {
	return value / 2;
}

double FreeEMS::raw2EGO(uint16_t value) {
	return (value * 14.7) / 32768;
}

