#ifndef FreeEMS_h
#define FreeEMS_h
#include "../ECU.h"

class FreeEMS: public ECU {

public:
	void update();

	~FreeEMS();
	FreeEMS(HardwareSerial* serialPort);
private:
	uint8_t buffer[255];
	HardwareSerial* _serialPort;
	void processRawData(uint8_t * buffer, uint16_t length);

	void setData(uint8_t* buffer);
	int raw2Temp(uint16_t value);
	int raw2Percent(uint16_t value);
	int raw2MAP(uint16_t value);
	int raw2RPM(uint16_t value);
	double raw2EGO(uint16_t value);
};

#endif
